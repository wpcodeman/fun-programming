<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Profile;
use App\Models\User;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ProfileController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Profile(['user']), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user.name', '姓名');
            $grid->column('age')->display(function($age){
                return $age . '岁';
            });
            $grid->column('gender')->display(function($gender){
                return User::GENDER_OPTIONS[$gender];
            });

            //使用 json 字段进行排序
            $grid->column('options.price')->sortable('options->price');


            $grid->column('created_at')->sortable();
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Profile(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('age');
            $show->field('gender');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Profile(), function (Form $form) {
            //获取所有的用户信息
            $userModel = new User();
            $form->display('id');
            $form->select('user_id', '用户')->options($userModel->getAllUserOptions(['id', 'name'], 'id', 'name'));
            $form->number('age')->default(0)->min(0)->max(200);
            $form->radio('gender')->options(User::GENDER_OPTIONS)->default(1);
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
