<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Grid\RowAction\Star;
use App\Admin\Repositories\School;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class SchoolController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new School(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('name');
            $grid->column('student_count');
            $grid->column('student_works_count');
            $grid->column('teacher_count');
            $grid->column('teacher_works_count');

            //进度条（processBar）
            #$grid->rate->progressBar('success');
            $grid->column('rate', '进度条')->display(function (){
                return 50;
            })->progressBar('info', 'sm', 100);

            //展现 --树 dialog
            //showTreeDialog
            /*$nodes = (new $permissionModel)->allNodes();

            //传入二维数组 （未分层级）
            $grid->column('permissions')->showTreeInDialog($nodes);

            //也可以传入闭包
            $grid->column('permissions')->showTreeInDialog(function (Grid\Displayers\DialogTree $tree) use (&$nodes, $roleModel) {
                // 设置所有的节点
                $tree->nodes($nodes);

                // 设置节点数据字段名称， 默认 "id", "name", "parent_id"
                $tree->setIdColumn('id');
                $tree->setTitleColumn('title');
                $tree->setParentColumn('parent_id');

                // $this->>roles 可以获取当前行的字段值
                foreach (array_column($this->roles, 'slug') as $slug) {
                    if ($roleModel::isAdministrator($slug)){
                        //选中所有节点
                        $tree->checkAll();
                    }
                }
            });*/

            // 内容映射
            $grid->column('status')->display(function (){
                return 0;
            })->using([0 => '未激活', 1 => '正常']);
            // 第二个参数为默认值
            $grid->column('gender')->display(function (){
                return 1;
            })->using([1 => '男', 2 => '女']);

            //字符串分割为数组
            $grid->column('tag')->display(function (){
                return "你好，你好，你好";
            })->explode('，')->label();

            //prepend 方法用于给 string  或  array  类型的值前面插入内容
            //当一个值是字符串
            $grid->column('email')->display(function (){
                return "123@hp.com";
            })->prepend("mailto:");

            //当字段是一个数组
            $grid->column('arr')->display(function (){
                return [1, 2, 3];
            })->prepend("first item")->label();

            //append 方法用于给 string 或 array 类型值后面插入内容


            //字符串的截取
            $grid->column('content')->display(function (){
                return "你好啊，为什么呀，怎么回事呀，你在干什么呀，不懂呀，这是什么呀？？？";
            })->limit(10);

            //数组字段同样是支持的
            $grid->column('data')->display(function (){
                return [1, 2, 3];
            })->limit(2)->label();


            //列二维码
            $grid->column('website')->qrcode(function(){
                return 'dcat.wpeng123.cn';
            }, 200, 200);

            //可复制
            $grid->column('website')->display(function(){
                return 'dcat.wpeng123.cn';
            })->copyable();

            //可排序 (orderable)
            //通过 Column::orderable 可以开启可排序功能， 此功能需要在你的模型中  user ModelTree 或 useSortTrait, 并且
            // 需要继承  Spatie\EloquentSortable\Sortable 接口。


            //SortableTrait
            // 如果你的数据不是层级结构数据， 可以直接使用 use SortableTrait 更多用法可参考 eloquent-sortable。模型
            // 如果你的数据时层级结构数据， 可以直接使用 use Model 下面权限模型为例来演示用法
            //ModelTree 实际上也是集成了 eloquent-sortable 的功能


            //链接  Link
            //将字段显示为一个连接
            // link 方法不传参数时，链接的 href 和 text 都是当前列的值
            $grid->column('homepage')->display(function(){
                return "https://www.baidu.com";
            })->link();

            //传入闭包
            $grid->column('homepage123')->link(function(){
                return admin_url('users/');
            });

            //行操作 action
            //这个功能可以将某一列显示为 一个行操作的按钮， 比如下面所示是一个标星和取消标星的列操作，
            //点击这一列的星标图标之后，后台会切换字段的装填，页面图标也跟着切换，具体实现代码如下：


            $grid->column('star', '-')->action(Star::class);
            $grid->column('id', 'ID')->display(function(){
                return 123;
            })->sortable()->bold();


            ### 帮助方法
            // 字符串 操作
            // 如果当前输出数据为字符串， 那么可以通过链式方法调用  Illuminate\Support\Str 的方法。
            // 比如有下一列， 显示 title 字段的字符串值：
            #$grid->title();

            //在 title 列输出的字符串基础上调用 Str::title() 方法
            #$grid->title()->title();

            //调用方法之后输出还是字符串， 所以可以继续调用 Illuminate\Support\Str 的方法
            #$grid->title()->title()->ucfirst();
            #$grid->title()->title()->unfirst()->substr(1, 10);


            //数组操作
            //如果当初 输出的是数组，可以直接使用 链式调用 Illuminate\Support\Collect 方法
            // 比如  tag 列是从一对多关系取出来的数组数据

            $grid->tags()->display(function (){
              return array (
                  0 =>
                      array (
                          'id' => '16',
                          'name' => 'php',
                      ),
                  1 =>
                      array (
                          'id' => '17',
                          'name' => 'python',
                      ),
              );
            })->pluck('name')->map('ucwords')->implode('-');


            //混合使用
            //在上面两种类型方法中调用中 只要上一步输出是正确类型的值，便可以调用类型对应的方法，所以可以很灵活混合使用。
            //比如 images 字段是存储多图片地址数组的 JSON 格式的字符串类型：





            #### 扩展列的显示功能
            //可以通过两种方式扩展列功能， 第一种是通过匿名函数的方式
            //扩展列方法后 IDE 不会自动补全的，这时候可以通过 php artisan  admin:ide-helper 生成 IDE 提示文件
            //匿名函数
            // 在 app/Admin/bootstrap.php 加入以下代码

            //然后在 model-grid 中使用这个扩展：
            $grid->column('title')->display(function(){
                return 123;
            })->color('#CCFF66');


            //扩展类
            $grid->column('description')->popover('right');

            //指定列名
            #$grid->title();
            //指定状态
            #$grid->status();


            //##############  行的使用和扩展  ###############
            //启用或禁用默认操作按钮
            //model-grid 默认有四个操作 编辑、快捷编辑、删除、和详情，我们可以通过以下方式关闭它们：

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
                $actions->disableEdit();
                $actions->disableQuickEdit();
                $actions->disableView();
            });

            // 也可以通过以下方式启用或禁用按钮
            $grid->disableDeleteButton();
            $grid->disableEditButton();
            $grid->disableQuickEditButton();
            $grid->disableViewButton();


            //切换行操作按钮显示方式
            //全局默认行操作按钮显示方式可以通过配置参数 admin.grid.grid_action_class 参数进行配置，目前支持的行操作
            //按钮显示方式有以下两种
            // Dcat\Admin\Grid\Displayers\DropdownActions 下拉菜单方式
            // Dcat\Admin\Grid\Displayers\Actions 图标展示
            // Dcat\Admin\Grid\Displayers\ContextMenuActions 鼠标右键展示下拉菜单 Since V1.4.5

            //在配置文件里面设置
            #'grid_action_class' => Dcat\Admin\Grid\Displayers\DropdownAction::class,


            //在控制器中切换显示方式
            $grid->setActionClass(Grid\Displayers\Actions::class);



            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new School(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('student_count');
            $show->field('student_works_count');
            $show->field('teacher_count');
            $show->field('teacher_works_count');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new School(), function (Form $form) {
            $form->display('id');
            //学校名称 必须填写 + 字符串
            $form->text('name')->type('string')->minLength(2, '最少输入2个字')->maxLength(60, '不能超过60个字符')->required();
        });
    }
}
