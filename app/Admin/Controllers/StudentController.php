<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Student;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;

class StudentController extends AdminController
{
    //学生类型
    const TYPE_STUDENT = 1;

    //审核中
    const VERIFY_ING = 0;

    //审核通过
    const VERIFY_AGREE = 1;

    //审核拒绝
    const VERIFY_REFUSE = 2;

    //默认密码
    const DEFAULT_PASSWORD = 123456;

    //性别的数组常量
    const SEX_OPTIONS = [
        1 => '男',
        2 => '女'
    ];

    //老师&学生类型数组常量
    const TYPE_OPTIONS = [
        1 => '学生',
        2 => '老师'
    ];

    //年级的数组常量
    const GRADE_OPTIONS = [
        1 => '1年级',
        2 => '2年级',
        3 => '3年级',
        4 => '4年级',
        5 => '5年级',
        6 => '6年级',
        7 => '7年级',
        8 => '8年级',
        9 => '9年级',
    ];

    //班级的数组常量
    const CLASS_OPTIONS = [
        1 => '1班',
        2 => '2班',
        3 => '3班',
        4 => '4班',
        5 => '5班',
        6 => '6班',
        7 => '7班',
        8 => '8班',
        9 => '9班',
    ];

    /**
     * 获取标题
     * @param int $is_verify
     * @return string
     */
    public function title($is_verify = self::VERIFY_AGREE)
    {
        if ($is_verify == self::VERIFY_AGREE) {
            return '学生列表';
        } else {
            return '审核列表';
        }
    }

    /**
     * 学生列表
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->title($this->title(self::VERIFY_AGREE))
            ->body($this->grid(self::VERIFY_AGREE));
    }

    /**
     * 学生审核列表
     * @param Content $content
     * @return Content
     */
    public function verify(Content $content)
    {
        return $content
            ->title($this->title(self::VERIFY_ING))
            ->body($this->grid(self::VERIFY_ING));
    }

    /**
     * 通过审核操作
     */
    public function agree()
    {
        echo 123; exit;
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($is_verify)
    {
        //获取性别选项
        $sexOptions = self::SEX_OPTIONS;

        return Grid::make(new Student(), function (Grid $grid) use ($sexOptions, $is_verify){
            $grid->column('id')->sortable();
            $grid->column('name', '学生名称');
            $grid->column('account');
            $grid->column('password_text');
            $grid->column('school_name');
            $grid->column('grade');
            $grid->column('class');
            $grid->column('mobile');
            $grid->column('gender')->display(function($gender) use ($sexOptions){
                return $sexOptions[$gender];
            });
            $grid->column('birthday');
            $grid->column('created_at', '注册时间')->sortable();

            //设置默认增加一个学生条件
            $grid->model()->where('type', '=', self::TYPE_STUDENT);
            //设置表格带边框的样式
            $grid->withBorder();
            //禁止工具栏默认显示 outline 模式
            $grid->toolsWithOutline(false);
            //增加审核状态的判断
            $grid->model()->where('is_verify', '=', $is_verify);

            //如果是审核列表取消一些 button 的显示
            if ($is_verify == self::VERIFY_ING) {
                // 禁用
                $grid->disableCreateButton();
                // 禁用批量删除按钮
                $grid->disableBatchDelete();
                $grid->tools('<a class="btn btn-primary disable-outline"  src="' . route('student-agree'). '">通过</a>');
            }

            $grid->filter(function (Grid\Filter $filter) {
                //修改查询布局方式
                $filter->panel();

                //或者非常模糊的查询
                $filter->where('search', function ($query) {
                    $query->where('name', 'like', "%{$this->input}%")
                        ->orWhere('account', '=', $this->input)
                        ->orWhere('mobile', '=', $this->input)
                        ->orWhere('school_name', '=', $this->input)
                        ->orWhere('grade_name', '=', $this->input)
                        ->orWhere('class_name', '=', $this->input);
                })->placeholder('姓名、账号、手机号、学校、年级、班级...');

            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Student(), function (Show $show) {
            $show->field('id');
            $show->field('type');
            $show->field('name');
            $show->field('account');
            $show->field('password');
            $show->field('password_text');
            $show->field('school_id');
            $show->field('school_name');
            $show->field('subject');
            $show->field('grade');
            $show->field('class');
            $show->field('mobile');
            $show->field('gender');
            $show->field('birthday');
            $show->field('is_verify');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        //获取所有学校的数据
        $schoolData = $this->getSchoolOptions();

        //获取所有老师的数据
        $teacherData = $this->getTeacherOptions();

        //使用闭包函数的 use 特性进行传参
        return Form::make(new Student(), function (Form $form) use($schoolData, $teacherData) {
            $form->display('id');
            $form->text('name')->type('string')->minLength(2, '最少输入2个字')->maxLength(30, '不能超过30个字符')->required();
            $form->select('school_id', '学校')->options($schoolData)->required();
            $form->select('teacher_id')->options($teacherData)->required();
            $form->select('grade')->options(self::GRADE_OPTIONS)->required();
            $form->select('class')->options(self::CLASS_OPTIONS)->required();
            $form->text('mobile')->type('number')->minLength(11, '最少输入11个数字')->maxLength(11, '不能超过11个数字')->required();
            $form->select('gender')->options(self::SEX_OPTIONS)->required();
            $form->date('birthday')->required();
            $form->display('created_at');
            $form->display('updated_at');

            //获取表单提交的数据
            $form->saving(function (Form $from) use($schoolData) {
                //设置类型为 type = 1 学生
                $from->model()->type = self::TYPE_STUDENT;
                //增加修改学校的名称
                $from->model()->school_name = $schoolData[$from->input('school_id')];
                //增加修改的年级
                $from->model()->grade_name = isset(self::GRADE_OPTIONS[$from->input('grade_name')]) ? self::GRADE_OPTIONS[$from->input('grade_name')] : '';
                //增加修改的班级
                $from->model()->class_name = isset(self::CLASS_OPTIONS[$from->input('class_name')]) ? self::CLASS_OPTIONS[$from->input('class_name')] : '';

                //判断是否是创建用户
                if ($from->isCreating()) {
                    //随机生成密码 默认为：123456
                    $from->model()->password = bcrypt(self::DEFAULT_PASSWORD);
                    $from->model()->password_text = self::DEFAULT_PASSWORD;
                }
            });

            //进行数据的保存
            $form->saved(function (Form $from) {
                $from->updates();
            });
        });
    }

    /**
     * 获取学校的选择信息数据
     * @return array
     */
    protected function getSchoolOptions()
    {
        $schoolData = DB::table('schools')->select('id', 'name')->get()->toArray();
        if (empty($schoolData)) {
            return [];
        }
        //返回对应的学校选择信息
        return $this->map($schoolData, 'id', 'name');
    }

    /**
     * 获取老师的选择信息数据
     * @return array
     */
    protected function getTeacherOptions()
    {
        $teacherData = DB::table('student_teachers')->select('id', 'name')->get()->toArray();
        if (empty($teacherData)) {
            return [];
        }
        //返回对应的选择信息
        return $this->map($teacherData, 'id', 'name');
    }

    /**
     * 处理 key & value 数据
     * @param $array
     * @param $from
     * @param $to
     * @return array
     */
    public function map($array, $from, $to)
    {
        $arr = [];
        foreach ($array as $item) {
            $arr[$item->$from] = $item->$to;
        }
        return $arr;
    }

}
