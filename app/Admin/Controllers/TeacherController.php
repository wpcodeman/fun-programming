<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Teacher;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;
use Illuminate\Support\Facades\DB;
use Dcat\Admin\Http\Controllers\AdminController;

class TeacherController extends AdminController
{
    //老师类型
    const TYPE_TEACHER = 2;

    //审核中
    const VERIFY_ING = 0;

    //审核通过
    const VERIFY_AGREE = 1;

    //审核拒绝
    const VERIFY_REFUSE = 2;

    //默认密码
    const DEFAULT_PASSWORD = 123456;

    //性别的数组常量
    const SEX_OPTIONS = [
        1 => '男',
        2 => '女'
    ];

    const SUBJECT_OPTIONS = [
        1 => '信息技术',
        2 => '体/美/音',
        3 => '自然/科学',
        4 => '语/数/外',
    ];

    /**
     * 获取标题
     * @param int $is_verify
     * @return string
     */
    public function title($is_verify = self::VERIFY_AGREE)
    {
        if ($is_verify == self::VERIFY_AGREE) {
            return '老师列表';
        } else {
            return '审核列表';
        }
    }

    /**
     * 老师列表
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->title($this->title(self::VERIFY_AGREE))
            ->body($this->grid(self::VERIFY_AGREE));
    }

    /**
     * 老师审核列表
     * @param Content $content
     * @return Content
     */
    public function verify(Content $content)
    {
        return $content
            ->title($this->title(self::VERIFY_ING))
            ->body($this->grid(self::VERIFY_ING));
    }

    /**
     * 通过审核操作
     */
    public function agree()
    {
        echo 123; exit;
    }

    /**
     * Make a grid builder.
     * @param $is_verify
     * @return Grid
     */
    protected function grid($is_verify = self::VERIFY_AGREE)
    {
        //获取性别选项
        $sexOptions = self::SEX_OPTIONS;
        //获取教学科目选项
        $subjectOptions = self::SUBJECT_OPTIONS;

        return Grid::make(new Teacher(), function (Grid $grid) use($sexOptions, $subjectOptions, $is_verify) {
            $grid->column('id')->sortable();
            $grid->column('name', '老师名称');
            $grid->column('account');
            $grid->column('password_text');
            $grid->column('school_name');
            $grid->column('subject')->display(function($subject) use ($subjectOptions){
                return $subjectOptions[$subject];
            });
            $grid->column('mobile');
            $grid->column('gender')->display(function($gender) use ($sexOptions){
                return $sexOptions[$gender];
            });
            $grid->column('birthday');
            $grid->column('created_at', '注册时间')->sortable();

            //设置默认增加一个老师条件
            $grid->model()->where('type', '=', self::TYPE_TEACHER);
            //增加审核状态的判断
            $grid->model()->where('is_verify', '=', $is_verify);


            #$grid->addTableClass(['table-text-center']);


            //如果是审核列表取消一些 button 的显示
            if ($is_verify == self::VERIFY_ING) {
                // 禁用
                $grid->disableCreateButton();
                // 禁用批量删除按钮
                $grid->disableBatchDelete();
                $grid->tools('<a class="btn btn-primary disable-outline"  src="' . route('teacher-agree'). '">通过</a>');
            }

            //设置表格带边框的样式
            $grid->withBorder();
            //禁止工具栏默认显示 outline 模式
            $grid->toolsWithOutline(false);

            $grid->filter(function (Grid\Filter $filter) {
                //修改查询布局方式
                $filter->panel();

                //或者非常模糊的查询
                $filter->where('search', function ($query) {
                    $query->where('name', 'like', "%{$this->input}%")
                        ->orWhere('account', '=', $this->input)
                        ->orWhere('mobile', '=', $this->input)
                        ->orWhere('school_name', '=', $this->input)
                        ->orWhere('subject_name', '=', $this->input);
                })->placeholder('姓名、账号、手机号、学校、所教科目...');
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Teacher(), function (Show $show) {
            $show->field('id');
            $show->field('type');
            $show->field('name');
            $show->field('account');
            $show->field('password_text');
            $show->field('school_name');
            $show->field('subject_name');
            $show->field('mobile');
            $show->field('gender');
            $show->field('birthday');
            $show->field('created_at', '注册时间');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        //获取所有学校的数据
        $schoolData = $this->getSchoolOptions();
        //获取学科的数据
        $subjectData = self::SUBJECT_OPTIONS;

        return Form::make(new Teacher(), function (Form $form) use($schoolData, $subjectData) {
            $form->display('id');
            //如果是修改的时候
            if ($form->isEditing()) {
                $form->display('account');
            } else {
                $form->text('account')->type('string')->minLength(6, '最少输入6个字符')->maxLength(30, '不能超过30个字符')->required();
            }
            $form->text('name')->type('string')->minLength(2, '最少输入2个字')->maxLength(30, '不能超过30个字')->required();
            $form->select('school_id', '学校')->options($schoolData)->required();
            $form->select('subject')->options($subjectData)->required();
            $form->text('mobile')->type('number')->minLength(11, '最少输入11个数字')->maxLength(11, '不能超过11个数字')->required();;
            $form->select('gender')->options(self::SEX_OPTIONS)->required();
            $form->date('birthday')->required();
            $form->display('created_at');
            $form->display('updated_at');

            //获取表单提交的数据
            $form->saving(function (Form $from) use($schoolData, $subjectData) {
                //设置类型为 type = 2 老师
                $from->model()->type = self::TYPE_TEACHER;
                //增加修改学校的名称
                $from->model()->school_name = isset($schoolData[$from->input('school_id')]) ? $schoolData[$from->input('school_id')] : '';
                //增加修改的课程名称
                $from->model()->subject_name = isset($subjectData[$from->input('subject')]) ? $subjectData[$from->input('subject')] : '';

                //判断是否是创建用户
                if ($from->isCreating()) {
                    //随机生成密码 默认为：123456
                    $from->model()->password = bcrypt(self::DEFAULT_PASSWORD);
                    $from->model()->password_text = self::DEFAULT_PASSWORD;
                }
            });

            //进行数据的保存
            $form->saved(function (Form $from) {
               $from->updates();
            });
        });
    }

    /**
     * 获取学校的选择信息数据
     * @return array
     */
    protected function getSchoolOptions()
    {
        $schoolData = DB::table('schools')->select('id', 'name')->get()->toArray();
        if (empty($schoolData)) {
            return [];
        }
        //返回对应的学校选择信息
        return $this->map($schoolData, 'id', 'name');
    }

    /**
     * 处理 key & value 数据
     * @param $array
     * @param $from
     * @param $to
     * @return array
     */
    public function map($array, $from, $to)
    {
        $arr = [];
        foreach ($array as $item) {
            $arr[$item->$from] = $item->$to;
        }
        return $arr;
    }

}
