<?php
namespace App\Admin\Extensions;

use Dcat\Admin\Admin;
use Dcat\Admin\Grid\Displayers\AbstractDisplayer;

class Popover extends AbstractDisplayer
{
    /**
     * 显示的方法
     * @param string $placement
     * @return mixed|void
     */
    public function display($placement = 'left')
    {
        //增加 jquery 脚本方法
        Admin::script("$('[data-toggle=\"popover\"]').popover()");

        return <<<EOF
    <button
    type="button"
    class="btn btn-secondary"
    title="popover"
    data-container="body"
    data-toggle="popover"
    data-placement="$placement"
    data-content="{$this->value}">
    弹出提示
</button>
EOF;


    }
}
