<?php
namespace App\Admin\Renderable;

use App\Models\Post as PostModel;
use Dcat\Admin\Support\LazyRenderable;
use Dcat\Admin\Widgets\Table;

class Post extends LazyRenderable
{
    //实现抽象类的抽象方法
    public function render()
    {
        //获取 ID
        $id = $this->key;

        //获取其他的自定义参数
        $type = $this->post_type;

        $data = PostModel::where('user_id', $id)
            ->where('type', $type)
            ->get(['title', 'body', 'body', 'created_at'])
            ->toArray();

        //返回的字段数组
        $titles = [
            'User ID',
            'Title',
            'Body',
            'Created At'
        ];

        return Table::make($titles, $data);
    }
}

