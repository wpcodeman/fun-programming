<?php

namespace App\Admin\Repositories;

use App\Models\Teacher as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Teacher extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
