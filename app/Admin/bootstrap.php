<?php

use App\Grid\Column\MyHeader;
use Dcat\Admin\Admin;
use Dcat\Admin\Grid;
use Dcat\Admin\Form;
use Dcat\Admin\Grid\Column;
use Dcat\Admin\Grid\Filter;
use Dcat\Admin\Show;

/**
 * Dcat-admin - admin builder based on Laravel.
 * @author jqh <https://github.com/jqhph>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 *
 * extend custom field:
 * Dcat\Admin\Form::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Column::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Filter::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

//统一设置 grid 里面的样式
Grid::resolving(function (Grid $grid) {
    //设置整体的条目的间距样式
    $grid->tableCollapse(false);

    //设置整体的表格为边框的样式
    $grid->withBorder();

    //设置整体的工具栏为 outline 模式
    $grid->toolsWithOutline(false);

    //内容居中
    $grid->addTableClass(['table-text-center']);


    //固定列不滚动
    $grid->fixColumns(1, -1);

});


//$value 是当前字段的值
//$p1、$p2 是自定义参数
Grid\Column::macro('myHeader', function ($value, $p1, $p2 = null){
    //myHeader 需要实现 Illuminate\Contracts\Support\Renderable 接口
    //当然这里可以直接传字符串
    return $this->addHeader(new MyHeader($this, $p1, $p2));
});

#### 扩展列的显示功能
Column::extend('color', function ($value, $color) {
    return "<span style='color: {$color}'>{$value}</span>";
});


//然后在 bootstrap 注册扩展类：
Column::extend('popover', \App\Admin\Extensions\Popover::class);


//指定列明
//除了上述两种扩展列功能，我们还可以通过指定列名称的方式扩展列功能
//在 app/Admin/bootstrap.php 加入以下代码：
/*Column::define('title', function ($value){
    return "<sapn style='color:red'>{$value}</span>";
});*/

//这个扩展方法等同于
#Column::define('statue', Dcat\Admin\Grid\Displayers\SwitchDisplay::class);
