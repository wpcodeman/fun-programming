<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    //添加平台用户的资源路由
    $router->resource('users', 'UserController');

    //添加学校管理的路由
    $router->resource('schools', 'SchoolController');

    //小的路由要加在 resource 的上面
    $router->get('students/verify', 'StudentController@verify');
    //审核通过逻辑
    $router->post('students/agree', 'StudentController@agree')->name('student-agree');
    $router->resource('students/verify', 'StudentController')->except(['index']);
    //添加学生管理的路由
    $router->resource('students', 'StudentController');


    //小的路由要加在 resource 的上面
    $router->get('teachers/verify', 'TeacherController@verify');
    //审核通过逻辑
    $router->post('teachers/agree', 'TeacherController@agree')->name('teacher-agree');
    $router->resource('teachers/verify', 'TeacherController')->except(['index']);
    //添加老师管理的路由
    $router->resource('teachers', 'TeacherController');



    //-------------> 一对多的关系的路由
    //profile 的路由
    $router->resource('profiles', 'ProfileController');



});
