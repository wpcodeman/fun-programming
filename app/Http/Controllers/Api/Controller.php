<?php
namespace App\Http\Controllers\Api;

use App\Api\Helpers\ApiResponse;
use App\Http\Controllers\Controller as BaseController;
use PHPUnit\Framework\MockObject\Api;

class Controller extends BaseController
{
    //使用 apiResponse 的方法
    use ApiResponse;
    //其他通用的 Api 帮助函数
}
