<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\UserRequest;
use App\Http\Resources\APi\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * 用户列表
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection | array
     */
    public function index()
    {
        //3个用户为一列
        $users = User::paginate(3);
        return $this->success($users);

        //返回用户资源列表
        /*$users = User::paginate(3);
        return UserResource::collection($users);*/
    }

    /**
     * 用户详情
     * @param User $user
     * @return array
     */
    public function show(User $user)
    {
        return $this->success(new UserResource($user));
    }

    /**
     * 用户注册
     * @param UserRequest $request
     * @return array
     * @throws \Exception
     */
    public function store(UserRequest $request)
    {
        User::create($request->all());
        return $this->setStatusCode(201)->success('用户注册成功！');
    }

    /**
     * 用户登录
     * @param Request $request
     * @return array|mixed
     */
    public function login(Request $request)
    {
        //这里面有啥时错的——> 看密码字段  password  写成了 psssword 无语~~~
        #$res = Auth::guard('web')->attempt(['name' => $request->name, 'password' => $request->psssword]);
        #$res = Auth::guard('web')->attempt(['name' => $request->name, 'password' => $request->password]);
        /*$res = Auth::guard('web')->attempt(['name' => $request->name, 'password' => $request->password]);
        if ($res){
            return $this->setStatusCode(201)->success('用户登录成功！');
        }*/

        //使用 jwt 来进行数据的返回
        $token = Auth::guard('api')->attempt(['name' => $request->name, 'password' => $request->password]);
        if ($token) {
            return $this->setStatusCode(201)->success(['token' => 'bearer ' . $token]);
        }

        return $this->failed('用户登录失败', 401);
    }

    //用户退出
    public function logout(){
        Auth::guard('api')->logout();
        return $this->success('退出成功...');
    }

    //返回当前登录用户信息
    public function info(){
        $user = Auth::guard('api')->user();
        return $this->success(new UserResource($user));
    }
}
