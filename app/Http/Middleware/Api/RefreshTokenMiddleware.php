<?php

namespace App\Http\Middleware\Api;

use Illuminate\Support\Facades\Auth;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class RefreshTokenMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //检查此次请求是否带有 token，如果没有抛出异常
        $this->checkForToken($request);

        //使用 try 包裹，捕捉 token 过期所抛出的 TokenException 异常
        try{
            //检测用户的登录状态, 正常则通过
            if ($this->auth->parseToken()->authenticate()) {
                return $next($request);
            }
            throw new UnauthorizedHttpException('jwt-auth', '未登录');
        } catch (TokenExpiredException $exception){
            //此处捕获到了 token 过期所抛出的 TokenExpiredExpiredException 异常，我们在这里做的是刷新该用户
            //的 token 并将它添加到相应头中。
            try{
                //刷新用户的 Token
                $token = $this->auth->refresh();
                //使用一次性登录保证此请求的成功
                Auth::guard('api')->onceUsingId(
                    $this->auth->manager()->getPayloadFactory()->buildClaimsCollection()->toPlainArray()['sub']
                );
            } catch (JWTException $exception) {
                //如果捕获到此异常， 即代表 refresh 也过期了， 用户无法刷新令牌，需要重新登录
                throw new UnauthorizedHttpException('jwt-auth', $exception->getMessage());
            }
        }
        //在相应头中返回新的 token
        return $this->setAuthenticationHeader($next($request), $token);
    }
}
