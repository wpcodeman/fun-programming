<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest as FormRequests;

class FormRequest extends FormRequests
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //应为验证器默认的权限验证是 false， 导致返回的都是 403 的权限不通过错误。这里我们咩有用到权限认证
        //为了方便处理，我们默认将权限都是通过桩体。所以每个文件我们都要将  false 该成 true
        #return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
