<?php

namespace App\Http\Resources\APi;

use App\Models\Enum\UserEnum;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /*switch ($this->is_verify){
            case -1:
                $this->is_verify = '已删除';
                break;
            case 0:
                $this->is_verify = '正常';
                break;
            case 1:
                $this->is_verify = '冻结';
                break;
        }*/

        return [
            'id' => $this->id,
            'name' => $this->name,
            #'is_verify' => $this->is_verify,
            'is_verify' => UserEnum::getStatusName($this->is_verify),
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
