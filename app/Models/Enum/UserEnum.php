<?php
namespace App\Models\Enum;

class UserEnum
{
    //审核中
    const VERIFY_ING = 0;
    //已同意
    const VERIFY_AGREE = 1;
    //已拒绝
    const VERIFY_REFUSE = 2;

    /**
     * 获取状态的名字
     * @param $status
     * @return string
     */
    public static function getStatusName($status)
    {
        switch ($status){
            case self::VERIFY_ING:
                return '审核中';
            case self::VERIFY_AGREE:
                return '已同意';
            case self::VERIFY_REFUSE:
                return '已拒绝，。';
            default:
                return '正常';
        }
    }
}
