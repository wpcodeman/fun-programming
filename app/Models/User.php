<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    #########################  现在的一些内容  ########################
    use Notifiable;
    /**
     * 定义表名称
     * @var string
     */
    protected $table = 'student_teachers';

    protected $fillable = [
        'name', 'password'
    ];

    //隐藏一些字段
    protected $hidden = [
        'password'
    ];

    //将密码进行加密
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    //缺少的字段  email 、email_verified_at、 remember_token

    /**
     * 用户注册方法
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public static function create($data)
    {

        try {
            DB::table('student_teachers')->insert([
                'name' => isset($data['name']) ? $data['name'] : '',
                'account' => isset($data['account']) ? $data['account'] : '',
                'password' => isset($data['password']) ? bcrypt($data['password']) : '',
                'birthday' => date('Y-m-d H:i:s')
            ]);
        } catch (\Exception $e){
            #var_dump($e->getMessage());exit;
            throw new \Exception($e->getMessage());
        }
        return true;
    }

    /**
     * 数据分页
     * @param $num
     * @return array
     */
    public static function paginate($num)
    {
        return DB::table('student_teachers')->limit(5)->get()->toArray();
    }

    /**
     * 获取 JWT 认证
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    #########################  现在的一些内容  ########################
    /**
     * 用户性别
     * @var string
     */
    const GENDER_OPTIONS = [
        1 => '男',
        2 => '女',
        3 => '未知',
    ];

    /**
     * 用户状态选项
     */
    const STATUS_OPTIONS = [
        0 => '未审核',
        1 => '审核中',
        2 => '已通过',
        3 => '已拒绝',
    ];

    /**
     * 定义一对一的模型
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id', 'id');
    }

    /**
     * 获取用户的 Options 数据
     * @param $column
     * @param $key
     * @param $value
     * @return array
     */
    public function getAllUserOptions($column, $key, $value)
    {
        return $this->map($this->getAllUserData($column), $key, $value);
    }

    /**
     * 获取所有用户信息列表
     * @param $column
     * @return array
     */
    public function getAllUserData($column)
    {
        if (empty($column)){
            return [];
        }
        return DB::table('users')->select($column)->get()->toArray();
    }

    /**
     * 获取对应的数据 map 数组
     * @param $array
     * @param $key
     * @param $value
     * @return array
     */
    public function map($array, $key, $value)
    {
        $arr = [];
        foreach ($array as $item) {
            $arr[$item->$key] = $item->$value;
        }
        return $arr;
    }

}
