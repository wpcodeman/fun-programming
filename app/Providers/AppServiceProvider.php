<?php

namespace App\Providers;


use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Laravel 5.4 默认数据库字符进行了更改，现在 utf8mb4 它包含了对存储表情符号的支持。
        //这只会影响新的应用程序，只要您运行 MySql5.7 及更改版本，就不需要做任何事情
        // 错误提示为： 语法错误或访问冲突：1071 指定秘钥太长；最大秘钥长度为  767字节
        //（ps：异常中明确指定最大长度为 767字节， utf8mb4 编码每个字符使用4个字节，所以 767/4 = 191.75, 所以将 string 的默认长度设置为 191 字符。希望对大家有帮助）
        Schema::defaultStringLength(191);
    }
}
