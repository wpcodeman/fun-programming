<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateStudentTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->unsigned()->default(0)->comment('类型 1学生 2老师');
            $table->string('name', 30)->default('')->comment('名字');
            $table->string('account', 30)->unique()->default('')->comment('登录账号');
            $table->string('password', 150)->default('')->comment('加密密码');
            $table->string('password_text', 50)->default('')->comment('名文密码');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name', 60)->default('')->comment('学校名称');
            $table->string('subject', 50)->default('')->comment('所教科目');
            $table->tinyInteger('grade')->unsigned()->default(0)->comment('年级');
            $table->tinyInteger('class')->unsigned()->default(0)->comment('班级');
            $table->char('mobile', 11)->default('')->comment('手机号码');
            $table->tinyInteger('gender')->unsigned()->default(0)->comment('性别类型 1男 2女');
            $table->date('birthday')->comment('出生年月');
            $table->tinyInteger('is_verify')->unsigned()->default(0)->comment('是否审核 1是 0否');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_teachers');
    }
}
