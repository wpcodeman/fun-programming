<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 60)->default('')->comment('学校名称');
            $table->integer('student_count')->default(0)->comment('学生数量');
            $table->integer('student_works_count')->default(0)->comment('老师作品数量');
            $table->integer('teacher_count')->default(0)->comment('老师数量');
            $table->integer('teacher_works_count')->default(0)->comment('老师作品数量');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
