<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->unsigned()->default(0)->comment('类型 1学生 2老师');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name', 60)->default('')->comment('学校名称');
            $table->integer('student_id')->unsigned()->default(0)->comment('学生编号');
            $table->string('student_name', 30)->default('')->comment('学生名称');
            $table->tinyInteger('grade')->unsigned()->default(0)->comment('年级');
            $table->tinyInteger('class')->unsigned()->default(0)->comment('班级');
            $table->integer('teacher_id')->unsigned()->default(0)->comment('老师编号');
            $table->string('teacher_name', 30)->default('')->comment('老师名称');
            $table->string('works_name', 100)->default('')->comment('作品名称');
            $table->string('works_des', 300)->default('')->comment('作品描述');
            $table->tinyInteger('is_approve')->unsigned()->default(0)->comment('是否审批 1是 0否');
            $table->tinyInteger('is_recommend')->unsigned()->default(0)->comment('推荐状态 1推荐 0未推荐');
            $table->integer('likes')->unsigned()->default(0)->comment('点赞数');
            $table->integer('comments')->unsigned()->default(0)->comment('评论数');
            $table->integer('views')->unsigned()->default(0)->comment('浏览数');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
