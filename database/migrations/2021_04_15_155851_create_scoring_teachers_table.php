<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateScoringTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scoring_teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('teacher_id')->unsigned()->default(0)->comment('老师编号');
            $table->string('teacher_name', 30)->default('')->comment('老师名称');
            $table->string('account', 30)->default('')->comment('登录账号');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name', 60)->default('')->comment('学校名称');
            $table->char('mobile', 11)->default('')->comment('手机号码');
            $table->integer('matches_count')->unsigned()->default(0)->comment('比赛数量');
            $table->integer('scoring_count')->unsigned()->default(0)->comment('打分作品数量');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scoring_teachers');
    }
}
