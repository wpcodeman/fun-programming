<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateWorksScoringListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_scoring_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('works_id')->unsigned()->default(0)->comment('作品编号');
            $table->string('works_name', 100)->comment('作品名称');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name')->default('')->comment('学校名称');
            $table->integer('student_id')->unsigned()->default(0)->comment('学生编号');
            $table->string('student_name', 30)->default('')->comment('学生名称');
            $table->tinyInteger('grade')->unsigned()->default(0)->comment('年级');
            $table->tinyInteger('class')->unsigned()->default(0)->comment('班级');
            $table->integer('score')->unsigned()->default(0)->comment('分数');
            $table->integer('scoring_school_id')->unsigned()->default(0)->comment('评分老师学校编号');
            $table->string('scoring_school_name', 60)->default('')->comment('评分老师学校名称');
            $table->integer('teacher_id')->unsigned()->default(0)->comment('评分老师编号');
            $table->string('teacher_name', 30)->default('')->comment('评分老师名称');
            $table->timestamp('scoring_at')->comment('评分时间');
            $table->integer('experience_score')->unsigned()->default(0)->comment('用户体验分');
            $table->integer('innovative_score')->unsigned()->default(0)->comment('创新构思分');
            $table->integer('artistic_score')->unsigned()->default(0)->comment('艺术审美分');
            $table->integer('procedural_score')->unsigned()->default(0)->comment('程序思维分');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_scoring_lists');
    }
}
