<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateWorksDeclareListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_declare_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ranks_name', 60)->comment('队伍名称');
            $table->integer('works_id')->unsigned()->default(0)->comment('作品编号');
            $table->string('works_name', 100)->comment('作品名称');
            $table->string('captain_name', 20)->comment('队长名称');
            $table->string('member_name', 150)->comment('队员名称');
            $table->string('device_type', 30)->comment('设备类型');
            $table->char('mobile', 11)->default('')->comment('手机号码');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name')->default('')->comment('学校名称');
            $table->integer('teacher_id')->unsigned()->default(0)->comment('辅导老师编号');
            $table->string('teacher_name', 30)->default('')->comment('辅导老师名称');
            $table->string('declare_name', 30)->default('')->comment('申报人');
            $table->timestamp('declare_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('申报时间');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_declare_lists');
    }
}
