<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CreateWorksDeclaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_declares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('declare_id')->unsigned()->default(0)->comment('申报编号');
            $table->string('group_name', 60)->comment('组名称');
            $table->string('tool_name', 60)->comment('开发工具');
            $table->integer('student_id')->unsigned()->default(0)->comment('学生编号');
            $table->string('student_name', 30)->default('')->comment('学生名称');
            $table->tinyInteger('grade')->unsigned()->default(0)->comment('年级');
            $table->tinyInteger('class')->unsigned()->default(0)->comment('班级');
            $table->integer('school_id')->unsigned()->default(0)->comment('学校编号');
            $table->string('school_name')->default('')->comment('学校名称');
            $table->integer('teacher_id')->unsigned()->default(0)->comment('辅导老师编号');
            $table->string('teacher_name', 30)->default('')->comment('辅导老师名称');
            $table->char('mobile', 11)->default('')->comment('手机号码');
            $table->string('remark', 200)->default('')->comment('备注');
            $table->string('code_url', 200)->default('')->comment('代码地址');
            $table->timestamp('created_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('注册时间');
            $table->timestamp('updated_at', 0)->default(DB::raw('CURRENT_TIMESTAMP'))->comment('创建时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_declares');
    }
}
