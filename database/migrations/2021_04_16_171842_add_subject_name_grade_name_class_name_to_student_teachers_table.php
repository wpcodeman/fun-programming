<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubjectNameGradeNameClassNameToStudentTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_teachers', function (Blueprint $table) {
            //增加课程名称
            $table->string('subject_name', 30)->default('')->comment('课程名称')->after('subject');
            //增加年级名称
            $table->string('grade_name', 30)->default('')->comment('年级名称')->after('grade');
            //增加班级名称
            $table->string('class_name', 30)->default('')->comment('班级名称')->after('class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_teachers', function (Blueprint $table) {
            //
        });
    }
}
