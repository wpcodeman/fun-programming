<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->integerIncrements('id')->comment('编号');
            $table->integer('author_id')->unsigned()->nullable(false)->default(0)->comment('作者编号');
            $table->string('title', 255)->default('')->comment('标题');
            $table->string('content', 255)->default('')->comment('内容');
            $table->integer('rate')->unsigned()->nullable(false)->default(0)->comment('比率');
            $table->timestamp('release_at', 0)->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('created_at', 0)->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at', 0)->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
