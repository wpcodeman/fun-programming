<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->integerIncrements('id')->comment('编号');
            $table->integer('user_id')->unsigned()->nullable(false)->default(0)->comment('用户编号');
            $table->tinyInteger('age')->unsigned()->nullable(false)->default(0)->comment('年龄');
            $table->tinyInteger('gender')->unsigned()->nullable(false)->default(0)->comment('性别');
            $table->timestamp('created_at', 0)->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at', 0)->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
