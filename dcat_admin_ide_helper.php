<?php

/**
 * A helper file for Dcat Admin, to provide autocomplete information to your IDE
 *
 * This file should not be included in your code, only analyzed by your IDE!
 *
 * @author jqh <841324345@qq.com>
 */
namespace Dcat\Admin {
    use Illuminate\Support\Collection;

    /**
     * @property Grid\Column|Collection id
     * @property Grid\Column|Collection name
     * @property Grid\Column|Collection type
     * @property Grid\Column|Collection version
     * @property Grid\Column|Collection detail
     * @property Grid\Column|Collection created_at
     * @property Grid\Column|Collection updated_at
     * @property Grid\Column|Collection is_enabled
     * @property Grid\Column|Collection parent_id
     * @property Grid\Column|Collection order
     * @property Grid\Column|Collection icon
     * @property Grid\Column|Collection uri
     * @property Grid\Column|Collection extension
     * @property Grid\Column|Collection permission_id
     * @property Grid\Column|Collection menu_id
     * @property Grid\Column|Collection slug
     * @property Grid\Column|Collection http_method
     * @property Grid\Column|Collection http_path
     * @property Grid\Column|Collection role_id
     * @property Grid\Column|Collection user_id
     * @property Grid\Column|Collection value
     * @property Grid\Column|Collection username
     * @property Grid\Column|Collection password
     * @property Grid\Column|Collection avatar
     * @property Grid\Column|Collection remember_token
     * @property Grid\Column|Collection connection
     * @property Grid\Column|Collection queue
     * @property Grid\Column|Collection payload
     * @property Grid\Column|Collection exception
     * @property Grid\Column|Collection failed_at
     * @property Grid\Column|Collection migration
     * @property Grid\Column|Collection batch
     * @property Grid\Column|Collection email
     * @property Grid\Column|Collection token
     * @property Grid\Column|Collection student_count
     * @property Grid\Column|Collection student_works_count
     * @property Grid\Column|Collection teacher_count
     * @property Grid\Column|Collection teacher_works_count
     * @property Grid\Column|Collection teacher_id
     * @property Grid\Column|Collection teacher_name
     * @property Grid\Column|Collection account
     * @property Grid\Column|Collection school_id
     * @property Grid\Column|Collection school_name
     * @property Grid\Column|Collection mobile
     * @property Grid\Column|Collection matches_count
     * @property Grid\Column|Collection scoring_count
     * @property Grid\Column|Collection password_text
     * @property Grid\Column|Collection subject
     * @property Grid\Column|Collection grade
     * @property Grid\Column|Collection class
     * @property Grid\Column|Collection gender
     * @property Grid\Column|Collection birthday
     * @property Grid\Column|Collection is_verify
     * @property Grid\Column|Collection email_verified_at
     * @property Grid\Column|Collection student_id
     * @property Grid\Column|Collection student_name
     * @property Grid\Column|Collection works_name
     * @property Grid\Column|Collection works_des
     * @property Grid\Column|Collection is_approve
     * @property Grid\Column|Collection is_recommend
     * @property Grid\Column|Collection likes
     * @property Grid\Column|Collection comments
     * @property Grid\Column|Collection views
     * @property Grid\Column|Collection ranks_name
     * @property Grid\Column|Collection works_id
     * @property Grid\Column|Collection captain_name
     * @property Grid\Column|Collection member_name
     * @property Grid\Column|Collection device_type
     * @property Grid\Column|Collection declare_name
     * @property Grid\Column|Collection declare_at
     * @property Grid\Column|Collection declare_id
     * @property Grid\Column|Collection group_name
     * @property Grid\Column|Collection tool_name
     * @property Grid\Column|Collection remark
     * @property Grid\Column|Collection code_url
     * @property Grid\Column|Collection score
     * @property Grid\Column|Collection scoring_school_id
     * @property Grid\Column|Collection scoring_school_name
     * @property Grid\Column|Collection scoring_at
     * @property Grid\Column|Collection experience_score
     * @property Grid\Column|Collection innovative_score
     * @property Grid\Column|Collection artistic_score
     * @property Grid\Column|Collection procedural_score
     *
     * @method Grid\Column|Collection id(string $label = null)
     * @method Grid\Column|Collection name(string $label = null)
     * @method Grid\Column|Collection type(string $label = null)
     * @method Grid\Column|Collection version(string $label = null)
     * @method Grid\Column|Collection detail(string $label = null)
     * @method Grid\Column|Collection created_at(string $label = null)
     * @method Grid\Column|Collection updated_at(string $label = null)
     * @method Grid\Column|Collection is_enabled(string $label = null)
     * @method Grid\Column|Collection parent_id(string $label = null)
     * @method Grid\Column|Collection order(string $label = null)
     * @method Grid\Column|Collection icon(string $label = null)
     * @method Grid\Column|Collection uri(string $label = null)
     * @method Grid\Column|Collection extension(string $label = null)
     * @method Grid\Column|Collection permission_id(string $label = null)
     * @method Grid\Column|Collection menu_id(string $label = null)
     * @method Grid\Column|Collection slug(string $label = null)
     * @method Grid\Column|Collection http_method(string $label = null)
     * @method Grid\Column|Collection http_path(string $label = null)
     * @method Grid\Column|Collection role_id(string $label = null)
     * @method Grid\Column|Collection user_id(string $label = null)
     * @method Grid\Column|Collection value(string $label = null)
     * @method Grid\Column|Collection username(string $label = null)
     * @method Grid\Column|Collection password(string $label = null)
     * @method Grid\Column|Collection avatar(string $label = null)
     * @method Grid\Column|Collection remember_token(string $label = null)
     * @method Grid\Column|Collection connection(string $label = null)
     * @method Grid\Column|Collection queue(string $label = null)
     * @method Grid\Column|Collection payload(string $label = null)
     * @method Grid\Column|Collection exception(string $label = null)
     * @method Grid\Column|Collection failed_at(string $label = null)
     * @method Grid\Column|Collection migration(string $label = null)
     * @method Grid\Column|Collection batch(string $label = null)
     * @method Grid\Column|Collection email(string $label = null)
     * @method Grid\Column|Collection token(string $label = null)
     * @method Grid\Column|Collection student_count(string $label = null)
     * @method Grid\Column|Collection student_works_count(string $label = null)
     * @method Grid\Column|Collection teacher_count(string $label = null)
     * @method Grid\Column|Collection teacher_works_count(string $label = null)
     * @method Grid\Column|Collection teacher_id(string $label = null)
     * @method Grid\Column|Collection teacher_name(string $label = null)
     * @method Grid\Column|Collection account(string $label = null)
     * @method Grid\Column|Collection school_id(string $label = null)
     * @method Grid\Column|Collection school_name(string $label = null)
     * @method Grid\Column|Collection mobile(string $label = null)
     * @method Grid\Column|Collection matches_count(string $label = null)
     * @method Grid\Column|Collection scoring_count(string $label = null)
     * @method Grid\Column|Collection password_text(string $label = null)
     * @method Grid\Column|Collection subject(string $label = null)
     * @method Grid\Column|Collection grade(string $label = null)
     * @method Grid\Column|Collection class(string $label = null)
     * @method Grid\Column|Collection gender(string $label = null)
     * @method Grid\Column|Collection birthday(string $label = null)
     * @method Grid\Column|Collection is_verify(string $label = null)
     * @method Grid\Column|Collection email_verified_at(string $label = null)
     * @method Grid\Column|Collection student_id(string $label = null)
     * @method Grid\Column|Collection student_name(string $label = null)
     * @method Grid\Column|Collection works_name(string $label = null)
     * @method Grid\Column|Collection works_des(string $label = null)
     * @method Grid\Column|Collection is_approve(string $label = null)
     * @method Grid\Column|Collection is_recommend(string $label = null)
     * @method Grid\Column|Collection likes(string $label = null)
     * @method Grid\Column|Collection comments(string $label = null)
     * @method Grid\Column|Collection views(string $label = null)
     * @method Grid\Column|Collection ranks_name(string $label = null)
     * @method Grid\Column|Collection works_id(string $label = null)
     * @method Grid\Column|Collection captain_name(string $label = null)
     * @method Grid\Column|Collection member_name(string $label = null)
     * @method Grid\Column|Collection device_type(string $label = null)
     * @method Grid\Column|Collection declare_name(string $label = null)
     * @method Grid\Column|Collection declare_at(string $label = null)
     * @method Grid\Column|Collection declare_id(string $label = null)
     * @method Grid\Column|Collection group_name(string $label = null)
     * @method Grid\Column|Collection tool_name(string $label = null)
     * @method Grid\Column|Collection remark(string $label = null)
     * @method Grid\Column|Collection code_url(string $label = null)
     * @method Grid\Column|Collection score(string $label = null)
     * @method Grid\Column|Collection scoring_school_id(string $label = null)
     * @method Grid\Column|Collection scoring_school_name(string $label = null)
     * @method Grid\Column|Collection scoring_at(string $label = null)
     * @method Grid\Column|Collection experience_score(string $label = null)
     * @method Grid\Column|Collection innovative_score(string $label = null)
     * @method Grid\Column|Collection artistic_score(string $label = null)
     * @method Grid\Column|Collection procedural_score(string $label = null)
     */
    class Grid {}

    class MiniGrid extends Grid {}

    /**
     * @property Show\Field|Collection id
     * @property Show\Field|Collection name
     * @property Show\Field|Collection type
     * @property Show\Field|Collection version
     * @property Show\Field|Collection detail
     * @property Show\Field|Collection created_at
     * @property Show\Field|Collection updated_at
     * @property Show\Field|Collection is_enabled
     * @property Show\Field|Collection parent_id
     * @property Show\Field|Collection order
     * @property Show\Field|Collection icon
     * @property Show\Field|Collection uri
     * @property Show\Field|Collection extension
     * @property Show\Field|Collection permission_id
     * @property Show\Field|Collection menu_id
     * @property Show\Field|Collection slug
     * @property Show\Field|Collection http_method
     * @property Show\Field|Collection http_path
     * @property Show\Field|Collection role_id
     * @property Show\Field|Collection user_id
     * @property Show\Field|Collection value
     * @property Show\Field|Collection username
     * @property Show\Field|Collection password
     * @property Show\Field|Collection avatar
     * @property Show\Field|Collection remember_token
     * @property Show\Field|Collection connection
     * @property Show\Field|Collection queue
     * @property Show\Field|Collection payload
     * @property Show\Field|Collection exception
     * @property Show\Field|Collection failed_at
     * @property Show\Field|Collection migration
     * @property Show\Field|Collection batch
     * @property Show\Field|Collection email
     * @property Show\Field|Collection token
     * @property Show\Field|Collection student_count
     * @property Show\Field|Collection student_works_count
     * @property Show\Field|Collection teacher_count
     * @property Show\Field|Collection teacher_works_count
     * @property Show\Field|Collection teacher_id
     * @property Show\Field|Collection teacher_name
     * @property Show\Field|Collection account
     * @property Show\Field|Collection school_id
     * @property Show\Field|Collection school_name
     * @property Show\Field|Collection mobile
     * @property Show\Field|Collection matches_count
     * @property Show\Field|Collection scoring_count
     * @property Show\Field|Collection password_text
     * @property Show\Field|Collection subject
     * @property Show\Field|Collection grade
     * @property Show\Field|Collection class
     * @property Show\Field|Collection gender
     * @property Show\Field|Collection birthday
     * @property Show\Field|Collection is_verify
     * @property Show\Field|Collection email_verified_at
     * @property Show\Field|Collection student_id
     * @property Show\Field|Collection student_name
     * @property Show\Field|Collection works_name
     * @property Show\Field|Collection works_des
     * @property Show\Field|Collection is_approve
     * @property Show\Field|Collection is_recommend
     * @property Show\Field|Collection likes
     * @property Show\Field|Collection comments
     * @property Show\Field|Collection views
     * @property Show\Field|Collection ranks_name
     * @property Show\Field|Collection works_id
     * @property Show\Field|Collection captain_name
     * @property Show\Field|Collection member_name
     * @property Show\Field|Collection device_type
     * @property Show\Field|Collection declare_name
     * @property Show\Field|Collection declare_at
     * @property Show\Field|Collection declare_id
     * @property Show\Field|Collection group_name
     * @property Show\Field|Collection tool_name
     * @property Show\Field|Collection remark
     * @property Show\Field|Collection code_url
     * @property Show\Field|Collection score
     * @property Show\Field|Collection scoring_school_id
     * @property Show\Field|Collection scoring_school_name
     * @property Show\Field|Collection scoring_at
     * @property Show\Field|Collection experience_score
     * @property Show\Field|Collection innovative_score
     * @property Show\Field|Collection artistic_score
     * @property Show\Field|Collection procedural_score
     *
     * @method Show\Field|Collection id(string $label = null)
     * @method Show\Field|Collection name(string $label = null)
     * @method Show\Field|Collection type(string $label = null)
     * @method Show\Field|Collection version(string $label = null)
     * @method Show\Field|Collection detail(string $label = null)
     * @method Show\Field|Collection created_at(string $label = null)
     * @method Show\Field|Collection updated_at(string $label = null)
     * @method Show\Field|Collection is_enabled(string $label = null)
     * @method Show\Field|Collection parent_id(string $label = null)
     * @method Show\Field|Collection order(string $label = null)
     * @method Show\Field|Collection icon(string $label = null)
     * @method Show\Field|Collection uri(string $label = null)
     * @method Show\Field|Collection extension(string $label = null)
     * @method Show\Field|Collection permission_id(string $label = null)
     * @method Show\Field|Collection menu_id(string $label = null)
     * @method Show\Field|Collection slug(string $label = null)
     * @method Show\Field|Collection http_method(string $label = null)
     * @method Show\Field|Collection http_path(string $label = null)
     * @method Show\Field|Collection role_id(string $label = null)
     * @method Show\Field|Collection user_id(string $label = null)
     * @method Show\Field|Collection value(string $label = null)
     * @method Show\Field|Collection username(string $label = null)
     * @method Show\Field|Collection password(string $label = null)
     * @method Show\Field|Collection avatar(string $label = null)
     * @method Show\Field|Collection remember_token(string $label = null)
     * @method Show\Field|Collection connection(string $label = null)
     * @method Show\Field|Collection queue(string $label = null)
     * @method Show\Field|Collection payload(string $label = null)
     * @method Show\Field|Collection exception(string $label = null)
     * @method Show\Field|Collection failed_at(string $label = null)
     * @method Show\Field|Collection migration(string $label = null)
     * @method Show\Field|Collection batch(string $label = null)
     * @method Show\Field|Collection email(string $label = null)
     * @method Show\Field|Collection token(string $label = null)
     * @method Show\Field|Collection student_count(string $label = null)
     * @method Show\Field|Collection student_works_count(string $label = null)
     * @method Show\Field|Collection teacher_count(string $label = null)
     * @method Show\Field|Collection teacher_works_count(string $label = null)
     * @method Show\Field|Collection teacher_id(string $label = null)
     * @method Show\Field|Collection teacher_name(string $label = null)
     * @method Show\Field|Collection account(string $label = null)
     * @method Show\Field|Collection school_id(string $label = null)
     * @method Show\Field|Collection school_name(string $label = null)
     * @method Show\Field|Collection mobile(string $label = null)
     * @method Show\Field|Collection matches_count(string $label = null)
     * @method Show\Field|Collection scoring_count(string $label = null)
     * @method Show\Field|Collection password_text(string $label = null)
     * @method Show\Field|Collection subject(string $label = null)
     * @method Show\Field|Collection grade(string $label = null)
     * @method Show\Field|Collection class(string $label = null)
     * @method Show\Field|Collection gender(string $label = null)
     * @method Show\Field|Collection birthday(string $label = null)
     * @method Show\Field|Collection is_verify(string $label = null)
     * @method Show\Field|Collection email_verified_at(string $label = null)
     * @method Show\Field|Collection student_id(string $label = null)
     * @method Show\Field|Collection student_name(string $label = null)
     * @method Show\Field|Collection works_name(string $label = null)
     * @method Show\Field|Collection works_des(string $label = null)
     * @method Show\Field|Collection is_approve(string $label = null)
     * @method Show\Field|Collection is_recommend(string $label = null)
     * @method Show\Field|Collection likes(string $label = null)
     * @method Show\Field|Collection comments(string $label = null)
     * @method Show\Field|Collection views(string $label = null)
     * @method Show\Field|Collection ranks_name(string $label = null)
     * @method Show\Field|Collection works_id(string $label = null)
     * @method Show\Field|Collection captain_name(string $label = null)
     * @method Show\Field|Collection member_name(string $label = null)
     * @method Show\Field|Collection device_type(string $label = null)
     * @method Show\Field|Collection declare_name(string $label = null)
     * @method Show\Field|Collection declare_at(string $label = null)
     * @method Show\Field|Collection declare_id(string $label = null)
     * @method Show\Field|Collection group_name(string $label = null)
     * @method Show\Field|Collection tool_name(string $label = null)
     * @method Show\Field|Collection remark(string $label = null)
     * @method Show\Field|Collection code_url(string $label = null)
     * @method Show\Field|Collection score(string $label = null)
     * @method Show\Field|Collection scoring_school_id(string $label = null)
     * @method Show\Field|Collection scoring_school_name(string $label = null)
     * @method Show\Field|Collection scoring_at(string $label = null)
     * @method Show\Field|Collection experience_score(string $label = null)
     * @method Show\Field|Collection innovative_score(string $label = null)
     * @method Show\Field|Collection artistic_score(string $label = null)
     * @method Show\Field|Collection procedural_score(string $label = null)
     */
    class Show {}

    /**
     
     */
    class Form {}

}

namespace Dcat\Admin\Grid {
    /**
     
     */
    class Column {}

    /**
     
     */
    class Filter {}
}

namespace Dcat\Admin\Show {
    /**
     
     */
    class Field {}
}
