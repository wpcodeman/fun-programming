<?php 
return [
    'labels' => [
        'Post' => '提交内容',
        'post' => '提交内容',
    ],
    'fields' => [
        'author_id' => '作者编号',
        'title' => '标题',
        'content' => '内容',
        'rate' => '比率',
        'release_at' => '发布时间',
    ],
    'options' => [
    ],
];
