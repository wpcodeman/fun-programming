<?php
return [
    'labels' => [
        'Profile' => '用户扩展',
        'profile' => '用户扩展',
    ],
    'fields' => [
        'user_id' => '用户编号',
        'age' => '年龄',
        'gender' => '性别',
        'options' => '属性',
    ],
    'options' => [
    ],
];
