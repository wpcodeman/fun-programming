<?php 
return [
    'labels' => [
        'School' => '学校管理',
        'school' => '学校管理',
    ],
    'fields' => [
        'name' => '学校名称',
        'student_count' => '学生数量',
        'student_works_count' => '老师作品数量',
        'teacher_count' => '老师数量',
        'teacher_works_count' => '老师作品数量',
    ],
    'options' => [
    ],
];
