<?php
return [
    'labels' => [
        'Student' => '学生管理',
        'student' => '学生管理',
    ],
    'fields' => [
        'type' => '类型',
        'name' => '真实名字',
        'account' => '登录账号',
        'password' => '加密密码',
        'password_text' => '密码',
        'school_id' => '学校编号',
        'school_name' => '学校名称',
        'subject' => '所教科目',
        'teacher_id' => '老师',
        'grade' => '年级',
        'grade_name' => '年级',
        'class' => '班级',
        'class_name' => '班级',
        'mobile' => '手机号码',
        'gender' => '性别',
        'birthday' => '出生年月',
        'is_verify' => '是否审核',
        'search' => '模糊查询',
    ],
    'options' => [
    ],
];
