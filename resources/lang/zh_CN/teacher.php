<?php
return [
    'labels' => [
        'Teacher' => '老师管理',
        'teacher' => '老师管理',
    ],
    'fields' => [
        'type' => '类型',
        'name' => '真实名字',
        'account' => '登录账号',
        'password' => '加密密码',
        'password_text' => '密码',
        'school_id' => '学校编号',
        'school_name' => '学校',
        'subject' => '所教科目',
        'subject_name' => '所教科目',
        'grade' => '年级',
        'class' => '班级',
        'mobile' => '手机号码',
        'gender' => '性别',
        'birthday' => '出生年月',
        'is_verify' => '是否审核',
        'search' => '模糊查询',
    ],
    'options' => [
    ],
];
