<?php
return [
    'labels' => [
        'User' => '平台用户',
        'user' => '平台用户',
    ],
    'fields' => [
        'name' => '用户名',
        'email' => '邮箱地址',
        'email_verified_at' => '邮箱验证时间',
        'password' => '密码',
        'remember_token' => '记住我Token',
        'status' => '状态',
    ],
    'options' => [
    ],
];
