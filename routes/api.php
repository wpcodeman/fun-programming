<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//专门写 Api 接口的路由，所以填写一个 index 做测试
Route::namespace('Api')->prefix('v1')->middleware('cors')->group(function() {
    //因为我们的 Api 控制器的命明空间是 App\Http\Controller\Api，而 Laravel 默认只会在命明空间 App\Http\Controllers 下查找控制器
    //所以需要我们给出 namespace
    //同时，添加一个 prefix 是为了版本号，方便后期接口升级区分。
    //打开 postman，用 get 方式请求 你的域名/api/v1/users, 最后返回结果是。
    Route::get('/users', 'UserController@index')->name('users.index');


    //添加刷新 token 的中间件
    Route::middleware('api.refresh')->group(function () {
        //获取当前用户信息的路由
        Route::get('/users/info','UserController@info')->name('users.info');
        //用户详情
        Route::get('/users/{user}', 'UserController@show')->name('users.show');

        //用户注册
        Route::post('/register', 'UserController@store')->name('users.store');
        //用户登录
        Route::post('/login', 'UserController@login')->name('users.login');
    });

});
//以上的所有返回结果，无论正确或者错误，都没有一个统一的格式规范，对开发 Api 不太友好，需要我们进行一些修改
//让 Laravel 框架可以编写更加友好的 Api
